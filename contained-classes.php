<?php
require_once 'vendor/autoload.php';

// Yeah, whatever, it's an example.
require_once 'lib/DatabaseConfiguration.php';
require_once 'lib/MailConfiguration.php';

/**
 * Example demonstrating the creation of contained classes.
 */

class ConfigurationContainedClasses
{
    use \Abivia\Configurable\Configurable;

    public $appName;
    public $database;
    public $mail;

    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'database' => ['className' => 'DatabaseConfiguration'],
            'mail' => ['className' => 'MailConfiguration'],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        return false;
    }

    protected function configurePropertyMap($property)
    {
        if ($property == 'application-name') {
            $property = 'appName';
        }
        return $property;
    }

}

ob_start();
$exampleName = 'contained-classes';
$obj = new ConfigurationContainedClasses();
$result = $obj->configure(json_decode(file_get_contents(dirname(__FILE__) . '/' . $exampleName . '.json')));
echo 'configure() returns ' . ($result ? 'true' : 'false') . "\n";
print_r($obj);
file_put_contents(dirname(__FILE__) . '/' . $exampleName . '.out.txt', ob_get_clean());