Examples for abivia/configurable
===

This is a set of examples that demonstrate some of the features of the Configurable trait.
See https://gitlab.com/abivia/configurable for the code.

Each example should have three files: the source JSON (example.json), the PHP source
(example.php), and the expected output (example.out.txt).

A few demonstration classes are in the lib/ folder.
