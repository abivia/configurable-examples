<?php
require_once 'vendor/autoload.php';

// Yeah, whatever, it's an example.
require_once 'lib/DatabaseConfiguration.php';

/**
 * Example demonstrating using an object property as an associative array key.
 */

class Environment
{
    use \Abivia\Configurable\Configurable;

    public $appName;
    public $database;

    protected function configureClassMap($property, $value)
    {
        static $classMap = [
            'database' => ['className' => 'DatabaseConfiguration', 'key' => 'key'],
        ];
        if (isset($classMap[$property])) {
            return (object) $classMap[$property];
        }
        return false;
    }

    protected function configurePropertyMap($property)
    {
        if ($property == 'application-name') {
            $property = 'appName';
        }
        return $property;
    }

}

ob_start();
$exampleName = 'associative-array';
$obj = new Environment();
$json = json_decode(file_get_contents(__DIR__ . '/' . $exampleName . '.json'));
$result = $obj->configure($json);
echo "original JSON\n";
print_r($json);
echo "\nconfigure() returns " . ($result ? 'true' : 'false') . "\n\n";
print_r($obj);
file_put_contents(__DIR__ . '/' . $exampleName . '.out.txt', ob_get_clean());