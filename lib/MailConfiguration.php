<?php

/**
 * A sample class for a mail server's configuration.
 */
class MailConfiguration {
    use \Abivia\Configurable\Configurable;

    protected $driver;
    protected $host;
    protected $from;
    protected $fromName;
    protected $name;
    protected $pass;
    protected $port;

    protected function configurePropertyMap($property) {
        if ($property == 'from-name') {
            $property = 'fromName';
        }
        return $property;
    }

    protected function configureValidate($property, &$value) {
        if ($property == 'driver') {
            $value = strtolower($value);
            return in_array($value, ['smtp', 'sendmail']);
        }
        return true;
    }

}

