<?php

/**
 *
 */
class DatabaseConfiguration {
    use \Abivia\Configurable\Configurable;

    protected $driver;
    protected $host;
    public $key;
    protected $label;
    protected $name;
    private $pass;
    protected $port;
    protected $user;

    protected function configureComplete() {
        // Move the label to the public key so it can be used as an array key.
        $this->key = $this->label;
        return true;
    }

    protected function configureValidate($property, &$value) {
        if ($property == 'driver') {
            $value = strtolower($value);
            return in_array($value, ['mysql', 'sqlserver']);
        }
        return true;
    }

}

